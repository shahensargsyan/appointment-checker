<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login')->name('login');
Route::any('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('user/activation/{token}', 'Api\ApiController@activateUser')->name('user.activate');
Route::get('booking/activation/{token}', 'Api\ApiController@activateBooking')->name('booking.activate');
Route::group(['middleware' => ['is_admin']], function () {
    Route::get('admin', 'AdminController@index')->name('admin');
    Route::get('addEvent', 'AdminController@addEvent')->name('addEvent');
    Route::get('google', 'AdminController@google')->name('google');
});
Route::get('token', 'SiteController@token')->name('token');
Route::post('events', 'SiteController@events')->name('events');

Route::group(['middleware' => ['is_user']], function () {
    Route::get('dashboard', 'SiteController@dashboard')->name('dashboard');
});

Route::get('/', 'SiteController@index')->name('index');

Route::post('register', 'SiteController@register')->name('register');
