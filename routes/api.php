<?php
header("Access-Control-Allow-Origin: {heck.fidem.am:5000}");
header('Access-Control-Allow-Credentials: true');
header('Access-Control-Max-Age: 86400');    // cache for 1 day
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");      
header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::get('test', 'Api\ApiController@test')->name('test');
Route::post('login', 'Api\ApiController@login')->name('login');

Route::post('addEvent', 'Api\ApiController@addEvent')->name('add_event');
Route::post('getEvents', 'Api\ApiController@getEvents')->name('get_event');
Route::post('getAvailableHoursForDate', 'Api\ApiController@getAvailableHoursForDate')->name('get_available_hours_for_date');
Route::get('getUserAvailableHoursForDate', 'Api\ApiController@getAvailableHoursForDate')->name('get_available_hours_for_date');

