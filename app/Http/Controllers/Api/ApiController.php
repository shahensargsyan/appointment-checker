<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use Request;
use App\User;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Oauth2;
use DateTime;
use DateTimeZone;
use App\GoogleClient;

class ApiController extends Controller
{

    protected $client;


    public function __construct() {
        $this->getGoogleClient();
    }
    
    function getGoogleClient() {
        if(!Request::has('id')) {
            return json_encode([
                        'error' => true,
                        'message' => 'Something wrong with input data',
                    ]);
            die;
        }
        $userId = Request::input('id');
        $user = User::with('googleUser')->where('id', $userId)->first();
        $google_user = $user->googleUser();
//        var_dump($user->googleUser->access_token);die;
        if(!$user){
           echo  json_encode([
                        'error' => true,
                        'message' => 'User not exist!',
                    ]);
           die;
        }
  
        $google = new GoogleClient();
        $this->client = $google->client;
        $this->client->setAccessToken($user->googleUser->access_token);
        if ($this->client->isAccessTokenExpired()) {
            $this->client->fetchAccessTokenWithRefreshToken($user->googleUser->access_token);
            $newAccessToken = $this->client->getAccessToken();
            $this->client->setAccessToken(\GuzzleHttp\json_encode($newAccessToken));

        }

        $this->client->authorize();
        
    }
    
    public function checkDateHourAvailablity($start,$end) {
        //checking date and hour availability
        
        $service = new Google_Service_Calendar($this->client);
        $events = $service->events->listEvents('primary', array('timeMin'=>$start, 'timeMax'=> $end));
        if(!empty($events->items)){
            echo json_encode([
                'error' => true,
                'message' => 'This hour is not available!',
            ]);
            die;
        }
        return TRUE;
        
        
    }


    public function addEvent() {
        if(!Request::has('name') || !Request::has('email') || !Request::has('phone')
                || !Request::has('startDate') || !Request::has('startTime')
                || !Request::has('endDate') || !Request::has('endTime')) {
            return response()->json([
                        'error' => true,
                        'message' => 'Something wrong with input data',
                    ]);
        }
        
        
        $service = new Google_Service_Calendar($this->client);
        $name = Request::input('name');
        $email = Request::input('email');
        $phone = Request::input('phone');
        $startDate = Request::input('startDate');
        $startTime = Request::input('startTime');
        $endDate = Request::input('endDate');
        $endTime = Request::input('endTime');
        
        if (!DateTime::createFromFormat('Y-m-d H:i:s', $startDate.' '.$startTime)) {
            return response()->json([
                'error' => true,
                'message' => 'Start Date or start time string does not match the Y-m-d H:i:s format'
            ]);
        }
        if (!DateTime::createFromFormat('Y-m-d H:i:s', $endDate.' '.$endTime)) {
            return response()->json([
                'error' => true,
                'message' => 'End Date or start time string does not match the Y-m-d H:i:s format'
            ]);
        }
        
        $time_zone = new DateTimeZone("America/New_York");//Asia/Yerevan
        
        $startDateTime = new DateTime($startDate.' '.$startTime, $time_zone);
        
        $startCalendarDateTime = new \Google_Service_Calendar_EventDateTime();
        $startCalendarDateTime->setDateTime($startDateTime->format(\DateTime::RFC3339));
        $startCalendarDateTime->setTimeZone("America/New_York");
 
         
        $endDateTime = new DateTime($startDate.' '.$startTime, $time_zone);
        $endDateTime->modify('+ 1 hour');
        $endCalendarDateTime = new \Google_Service_Calendar_EventDateTime();
        $endCalendarDateTime->setDateTime($endDateTime->format(\DateTime::RFC3339));
        $endCalendarDateTime->setTimeZone("America/New_York");

        $this->checkDateHourAvailablity($startDateTime->format(\DateTime::RFC3339),$endDateTime->format(\DateTime::RFC3339));

        $description = "phone:".$phone.', email:'.$email;
        $user_event = new Google_Service_Calendar_Event(array(
            'summary' => $name,
            'description' => $description,
            'start' => $startCalendarDateTime,
            'end' => $endCalendarDateTime,
            'attendees' => array(
                array('email' => $email)
            ),
        ));

        $calendarId = 'primary';
        $event = $service->events->insert($calendarId, $user_event);
        $json['success'] = true;
        $json['message'] = 'Event created:';
        $json['event'] = $event;
        
        
        return response()->json($json);
    }

    function getAvailableHoursForDate() {
        
        $service = new Google_Service_Calendar($this->client);
        
//        $calendar = $service->calendars->get('primary');
//        var_dump($calendar);die;
//        echo $calendar->getTimeZone();

        if(!Request::has('date')) {
            return response()->json([
                        'error' => true,
                        'message' => 'You have to send date with post params!',
                    ]);
        }
        $date = Request::input('date');
        if (!preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$date)) {
            return response()->json([
                        'error' => true,
                        'message' => 'Date format should be Y-m-d',
                    ]);
        }
        $time_zone = new DateTimeZone("America/New_York");//
        $startDateTime = new DateTime($date.' 09:00:00');
        $endDateTime = new DateTime($date.' 17:00:00');
        $events = $service->events->listEvents('primary', array('timeMin'=>$startDateTime->format(DateTime::ISO8601), 'timeMax'=> $endDateTime->format(DateTime::ISO8601)));
        $available = [];
        for($i=9;$i<17;$i++){
            $checkDate = new DateTime($date.' '.$i.':00:00',$time_zone);
            $available[] = $checkDate;
        }
        $reserved  = [];
        foreach ($events->items as $event) {
            $event_start =   new DateTime($event->start->dateTime,$time_zone);
            $event_end =  new DateTime($event->end->dateTime);
            $reserved[] = ['start' =>  new DateTime($event->start->dateTime),'end' =>  new DateTime($event->end->dateTime)];
            foreach ($available as $k=>$checkDate) {
                if($checkDate<$event_end && $checkDate>=$event_start){
                    unset($available[$k]);
                }
            }
        }
        $availableHours = [];
        foreach ($available as $value) {
            $availableHours[] = $value->format('H:i:s');
        }
        $json['success'] = true;
        $json['message'] = 'Available hours';
        $json['available_hours'] = $availableHours;


        return response()->json($json);
        
    }
    
    function getEvents() {
       
        if(!Request::has('month')) {
                $json['error'] = true;
                $json['message'] = 'You have to send month id with post data!';

                return response()->json($json);
        }
        $month = (int)Request::input('month')+1;
        $startDateTime = new DateTime(date('Y-'.$month.'-01').' 00:00:00');
        $endDateTime = new DateTime(date('Y-'.$month.'-t').' 24:00:00');
        
        $service = new Google_Service_Calendar($this->client);
        $events = $service->events->listEvents('primary', array('timeMin'=>$startDateTime->format(DateTime::ISO8601), 'timeMax'=> $endDateTime->format(DateTime::ISO8601)));

        
       
        if(!Request::has('calendar')) {
            return response()->json($this->eventsForFront($events));
        }else{
            return response()->json($this->eventsForCalendar($events));
        }


       
        
    }
    
    private function eventsForCalendar($events){
        $user_events  = [];
        foreach ($events->items as $event) {
            $start = new DateTime($event->start->dateTime);
            $end = new DateTime($event->end->dateTime);
            $event_start =   new DateTime($event->start->dateTime);
            $event_end =  new DateTime($event->end->dateTime);
            $user_events[] = [
                'start' =>  $start->format('Y-m-d'),
                'title' => $event->summary,
                'summary' => $event->summary,
            ];

        }
        return $user_events;
    }
    
    private function eventsForFront($events){
        $month = (int)Request::input('month')+1;
        $json['success'] = true;
        $json['message'] = 'User events for '.date("M", strtotime(date('Y-'.$month.'-01')));
        $user_events  = [];
        foreach ($events->items as $event) {
            $start = new DateTime($event->start->dateTime);
            $end = new DateTime($event->end->dateTime);
            $event_start =   new DateTime($event->start->dateTime);
            $event_end =  new DateTime($event->end->dateTime);
            $user_events[] = [
                'start' =>  [
                    'date' => $start->format('Y-m-d'),
                    'time' => $start->format('H:i:s')
                ],
                'end'  =>  [
                    'date' => $end->format('Y-m-d'),
                    'time' => $end->format('H:i:s')
                ],
                'displayName' => $event->creator->displayName,
                'email' => $event->creator->email,
                'description' => $event->description,
                'summary' => $event->summary,
            ];

        }
         $json['user_events'] = $user_events;
         
        return $json;
    }
}
