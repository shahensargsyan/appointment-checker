<?php

namespace App\Http\Controllers;

use Request;
use Validator;
use App\User;
use DB;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Oauth2;
use DateTime;
use Illuminate\Routing\Route;

class AdminController extends Controller
{
    protected $paginationCount = 20;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Route $route)
    {
//        if(!in_array($route->getActionName(), ['addEvent','token'])){
//            $this->middleware('auth');
            $this->middleware('is_admin', ['except' => ['index']]);
//        }
            
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = User::where('role', 'user')->get();
//        var_dump($data);
        return view('admin.users')
            ->with('data', $data);
        
    }
    
    
    public function addEvent(){
        
    }
    
    public function google(){
        $client = new Google_Client();
        $client->setAuthConfig('client_secret.json');
        $scopes = implode(' ',
            array(
                Google_Service_Calendar::CALENDAR,
                'email',
                'profile'
            ));
        $client->setScopes(
                $scopes
        );
        $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/token');
        $client->setAccessType('offline');        // offline access
        $client->setIncludeGrantedScopes(true);   // incremental auth
//        $client->setApprovalPrompt ("force");
        $auth_url = $client->createAuthUrl();
        header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        die;
    }
    
    public function token()
    {
        
        $client = new Google_Client();
        $client->setAuthConfig('client_secret.json');
        $scopes = implode(' ',
            array(
                Google_Service_Calendar::CALENDAR,
                'email',
                'profile'
            ));
        $client->setScopes($scopes);
        $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/token');
        $client->setIncludeGrantedScopes(true);   // incremental auth
        $client->authenticate($_GET['code']);
        $access_token = $client->getAccessToken();
        $refresh_token = $client->getRefreshToken();
        $google_oauth = new Google_Service_Oauth2($client);
        //$google_account_email = $google_oauth->userinfo->get()->email;
        $userData = $google_oauth->userinfo_v2_me->get();

        $google_oauth->userinfo->get()->familyName;
        $email = $google_oauth->userinfo->get()->email;
        $input["google_id"] = $userData->id;
        $input["email"] = $google_oauth->userinfo->get()->email;
        $input["first_name"] = $google_oauth->userinfo->get()->givenName;
        $input["last_name"] = $google_oauth->userinfo->get()->familyName;
        $input["username"] = $google_oauth->userinfo->get()->name;
        $input["access_token"] = json_encode($access_token);
        if($refresh_token)
            $input["refresh_token"] = json_encode($refresh_token);
        
        $user_check = User::where('email', $email)->whereNotNull('email')->first();
        if($user_check) {
            
            unset($input['password']);

            $user_check->update($input);
            $user = $user_check;
        } else {
            $user = User::create($input);
        }

    }
    
   
}
