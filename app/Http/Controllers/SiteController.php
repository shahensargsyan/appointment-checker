<?php

namespace App\Http\Controllers;

use Request;
use Validator;
use App\User;
use App\GoogleUser;
use Illuminate\Support\Facades\Auth;
use App\GoogleClient;
use Session;
class SiteController extends Controller
{
    protected $paginationCount = 20;
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('is_guest')->except('dashboard');
            
    }
    
    public function dashboard() {
        
        return view('dashboard');
    }

    public function index() {
        
        $data = ['action' => 'login'];
        
        $google = new GoogleClient();
        $google->setState($data);
        
        $auth_url = $google->client->createAuthUrl();
        return view('index')->with('auth_url',$auth_url);
        
    }
    
    public function register() {
        $rules = array(
            'first_name' => 'required',
            'last_name' => 'required',
            'company' => 'required'
        );
        $validator = Validator::make(Request::all(), $rules);
        if ($validator->fails()) {
            return redirect('/')
                ->withErrors($validator)->withInput();
        }
       
        $data = [
            'first_name' => Request::input('first_name'),
            'last_name' => Request::input('last_name'),
            'company' => Request::input('company'),
            'action' => 'register'
        ];
        
        $google = new GoogleClient();
        $google->setState($data);
        
        $auth_url = $google->client->createAuthUrl();
        header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL));
        die;
    }
    
    

    public function token() {
        
        $google = new GoogleClient();
        $authenticate =  $google->client->authenticate($_GET['code']);
        if(isset($authenticate['error'])){
              return redirect('/');
        }
        $userData = $google->getState();
        
        $userInfo = $google->getGoogleUserInfo();
//        var_dump($userData['action']);die;
        if($userData['action'] == 'register'){
            $access_token = $google->client->getAccessToken();
            $refresh_token = $google->client->getRefreshToken();

            $email = $userInfo->email;
            $googleUser["google_id"] = $userInfo->id;
            $googleUser["email"] = $userInfo->email;
            $googleUser["first_name"] = $userInfo->givenName;
            $googleUser["last_name"] = $userInfo->familyName;
            $googleUser["username"] = $userInfo->email;
            $googleUser["access_token"] = json_encode($access_token);
            if($refresh_token)
                $googleUser["refresh_token"] = json_encode($refresh_token);

            $google_user = GoogleUser::where('email', $email)->whereNotNull('email')->first();
            
            if(!$google_user) {
                $userData['email'] = $email;
                $userData['username'] = $email;
                $userData['password'] = bcrypt($userInfo->id);
                $user = User::create($userData);
                $googleUser['user_id'] = $user->id;
                $google_user = GoogleUser::create($googleUser);
            }
            
            $this->loginUser($userInfo->email,$userInfo->id);
        }elseif($userData['action'] == 'login'){
            $this->loginUser($userInfo->email,$userInfo->id);
        }
        return redirect('/');
    }
    public function events() {
        
        
    }
    
    private function loginUser($username,$password){
        $remember = TRUE;
        if(!Auth::attempt(['email' => $username, 'password' => $password], $remember)){
            Session::flash('message',  "Can't find user"); 
            Session::flash('alert-class', 'alert-danger'); 
        }else{
            Session::flash('message',  "Successfully logged in."); 
            Session::flash('success-class', 'alert-danger'); 
        }
    }
    
   
}
