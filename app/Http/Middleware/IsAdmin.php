<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin {

    public function handle($request, Closure $next)
    {
//        var_dump(Auths::user()->role);die;
        if ( Auth::check() && Auth::user()->role == 'admin' )
        {
            return $next($request);
        }

        return redirect('/');
    }
}