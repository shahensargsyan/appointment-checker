<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class IsGuest {

    public function handle($request, Closure $next)
    {
        if ( Auth::guest() )
        {
            return $next($request);
        }
//        var_dump(Auth::user()->role);die;
        if(Auth::user()->role == 'user'){
            return redirect('/dashboard');
        }else{
            return redirect('/admin');
        }
    }
}