<?php namespace App\Http\Middleware;

use Closure;
use Auth;

class IsUser {

    public function handle($request, Closure $next)
    {
        if ( Auth::check() && Auth::user()->role == 'user' )
        {
            return $next($request);
        }

        return redirect('/');
    }
}