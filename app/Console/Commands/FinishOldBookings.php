<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Booking;
use Carbon\Carbon;

class FinishOldBookings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'finish_old_bookings';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'All old opened Bookings will be finished after 8:00pm';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $bookings = Booking::where('status', 'Opened')->where('reserve_started_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))->where('reserve_finished_at', '<=', Carbon::now()->format('Y-m-d H:i:s'))->get();
        
        foreach($bookings as $booking)
        {
            $booking->update(['status' => 'Finished']);
        }
    }
}
