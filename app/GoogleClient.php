<?php

namespace App;

use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Oauth2;

class GoogleClient
{
    public $client;


    private function getScopes(){
        $scopes = implode(' ',
                array(
                    Google_Service_Calendar::CALENDAR,'email','profile'
                ));
        return $scopes;
    }
    
    public function __construct(){
       
        $this->client = new Google_Client();
        $this->client->setAuthConfig('client_secret.json');
        $this->client->setScopes($this->getScopes());

        $this->client->setAccessType('offline');        // offline access
        $this->client->setIncludeGrantedScopes(true);   // incremental auth
      
    }
    
    public function setState($data)
    {
        $inputStr = json_encode($data);
        $stateString = strtr(base64_encode($inputStr), '+/=', '-_,');
        $this->client->setState($stateString);
    }

    public function getState()
    {
        $json = base64_decode(strtr($_GET["state"], '-_,', '+/='));
        $data = json_decode($json,true);
        return $data;
    }
    
    public function getGoogleUserInfo(){
        $google_oauth = new Google_Service_Oauth2($this->client);
        
        return $google_oauth->userinfo->get();
    }
}
