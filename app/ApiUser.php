<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ApiUser extends Model
{
    protected $table = 'api_users';
	
    protected $fillable = [
		'user_id',
		'token',
		'last_login_at',
    ];
    
    public function user() {
        return $this->belongsTo(User::class);
    }
}
