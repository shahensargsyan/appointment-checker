<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class GoogleUser extends Model
{

    
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'google_id',
        'first_name',
        'last_name',
        'refresh_token',
        'access_token',
        'email',
        'user_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'refresh_token', 'access_token',
    ];
    
}
