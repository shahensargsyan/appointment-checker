<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {

    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'id' => '1',
            'username' => 'admin',
            'email' => 'admin@ff.com',
            'password' => Hash::make('123'),
            'role' => 'admin',
            'active' => 1
        ]);
    }
}
