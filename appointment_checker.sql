/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : appointment_checker

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2018-04-25 13:41:42
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api_users
-- ----------------------------
DROP TABLE IF EXISTS `api_users`;
CREATE TABLE `api_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `token` varchar(128) DEFAULT NULL,
  `last_login_at` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `api_users_token_unique` (`token`),
  KEY `api_users_user_id_foreign` (`user_id`),
  CONSTRAINT `api_users_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of api_users
-- ----------------------------

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2017_10_13_115718_create_api_users_table', '1');

-- ----------------------------
-- Table structure for password_resets
-- ----------------------------
DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of password_resets
-- ----------------------------

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `google_id` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `access_token` text NOT NULL,
  `refresh_token` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `birthday_at` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `role` enum('admin','user') NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_google_id_unique` (`google_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'Shahen Sargsyan', 'shahensar@gmail.com', '100849938274518106426', 'Shahen', 'Sargsyan', '{\"access_token\":\"ya29.GluoBYZdOG5_gbJsAt2KepPgXLaflFEhg2_U5Br7hFw0oZCF5B11VpR7dWAA8nl_rwSzMwDncxhRsigsncc7Hw1TZLPpkurvkOtnSQRpXjiWuR9F2w4hENVtxjLw\",\"token_type\":\"Bearer\",\"expires_in\":3599,\"id_token\":\"eyJhbGciOiJSUzI1NiIsImtpZCI6ImFmZmM2MjkwN2E0NDYxODJhZGMxZmE0ZTgxZmRiYTYzMTBkY2U2M2YifQ.eyJhenAiOiI3ODU1MzgzNjkxODAtMGJ2NnIzb2I1MWczbnJkNThqYTI2YjYzOXBnN3NsOHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI3ODU1MzgzNjkxODAtMGJ2NnIzb2I1MWczbnJkNThqYTI2YjYzOXBnN3NsOHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDA4NDk5MzgyNzQ1MTgxMDY0MjYiLCJlbWFpbCI6InNoYWhlbnNhckBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IndwdGJ3bFlOSzZ3RDFoQ1lYNnBJcXciLCJleHAiOjE1MjQ2NDgwNDksImlzcyI6Imh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbSIsImlhdCI6MTUyNDY0NDQ0OX0.VLKK_hBjDMqSqViGEMj9uNaQcq72Ksu-_OgEfmLEzMjzdZzZxEj_gZwpooSRL6lJ05A-0M3HhTG_Jg8nEM0Cv8j1muXJWmCmXwIi-1cZw2gusdmcarhXnsG9fDRHTlPbd44yNPRdLlGDl98AXL5L-ZUjGIqOF4JxUuyDlD_s1MawL5o7_qHAs0MlMiJJ3rKOLvpzckcBEoxHiwsKXYnWGc9bR1NDJ-YdwqcJpsc2260HpO6Y-MEysFVuUWlCulIDMMxRUZUgCIyTeUobbqzqAECvMc5OIqNsCwu02GsdYBTPRkX_CfIONewGQWeUbI6SFA2DgtCNEr7pn8o3y2RRgA\",\"created\":1524644448}', 'null', '', '1', null, null, 'user', null, '2018-04-25 01:20:50', '2018-04-25 01:20:50');
INSERT INTO `users` VALUES ('2', 'Shahen Sargsyan', 'sargsyanshahen@gmail.com', '109253147912313663362', 'Shahen', 'Sargsyan', '{\"access_token\":\"ya29.GluoBaUsr9TF56XFHrDAeAQ80CEByChG2AEa5cUDs0YeP2bwgomZIrwli1bb3Ewikxyr_SRNpGwh5XVF_JWXWzZ2k9GVdRi1VAfr7A0s4D_xGtQUFkm60x2bWa8V\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"id_token\":\"eyJhbGciOiJSUzI1NiIsImtpZCI6ImFmZmM2MjkwN2E0NDYxODJhZGMxZmE0ZTgxZmRiYTYzMTBkY2U2M2YifQ.eyJhenAiOiI3ODU1MzgzNjkxODAtMGJ2NnIzb2I1MWczbnJkNThqYTI2YjYzOXBnN3NsOHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI3ODU1MzgzNjkxODAtMGJ2NnIzb2I1MWczbnJkNThqYTI2YjYzOXBnN3NsOHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMDkyNTMxNDc5MTIzMTM2NjMzNjIiLCJlbWFpbCI6InNhcmdzeWFuc2hhaGVuQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoidG04UkZVUXlCajBNaDBhNWx3aGRidyIsImV4cCI6MTUyNDY1MDM1NCwiaXNzIjoiaHR0cHM6Ly9hY2NvdW50cy5nb29nbGUuY29tIiwiaWF0IjoxNTI0NjQ2NzU0fQ.KtnOvjPzg3_LOK2XSr8IH0U6JIh0UqwQ8oT88eqhgdqhiUUwOPTtOgSlrmZfdIxeOUupd6m26DcoMuwAdaBmqL4e9PXLZAjg-8j8898s9GqVqsfcWwx-mHc0SG6lgOYsiMaKET0XjoFXvUDCNldWoFVoLNoajQKYlsp_cRzbBSpaf5FLxuqRAVkDkpeRpX29jpiFpEoJkHGBgYvODbBeRMxMpIpAsI0__IFUFK0yEqvP36KqWFozib0CtWtFufkXGwrIOxcFtJ5WvUXNud2nGuYfgfdEcvjd2Gc868R2c9Xg76UrNj5nWfNS8s9UWwuJrzpxwQLUPqiMH--NMlRh8Q\",\"created\":1524646754}', 'null', '', '1', null, null, 'user', null, '2018-04-25 01:23:16', '2018-04-25 01:59:15');
INSERT INTO `users` VALUES ('3', 'Shahen Sargsyan', 'sargsyanshah@gmail.com', '114588942314700662182', 'Shahen', 'Sargsyan', '{\"access_token\":\"ya29.GluoBVVKWdboV3EnE81TY7FgMZQXbjLP3nC9usJhFZJQb8hd4xDB-O_OYGf6mU85Bj1J2q10eVihK5EfvUXItLY6jhat8kGSnyZw5giuoJtWW-pc3cjK_QkkMz76\",\"token_type\":\"Bearer\",\"expires_in\":3600,\"refresh_token\":\"1\\/2sdiDNA_TooBJDLicY8j_nybExA0BHQ-SYTR2gYIgso\",\"id_token\":\"eyJhbGciOiJSUzI1NiIsImtpZCI6ImFmZmM2MjkwN2E0NDYxODJhZGMxZmE0ZTgxZmRiYTYzMTBkY2U2M2YifQ.eyJhenAiOiI3ODU1MzgzNjkxODAtMGJ2NnIzb2I1MWczbnJkNThqYTI2YjYzOXBnN3NsOHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJhdWQiOiI3ODU1MzgzNjkxODAtMGJ2NnIzb2I1MWczbnJkNThqYTI2YjYzOXBnN3NsOHAuYXBwcy5nb29nbGV1c2VyY29udGVudC5jb20iLCJzdWIiOiIxMTQ1ODg5NDIzMTQ3MDA2NjIxODIiLCJlbWFpbCI6InNhcmdzeWFuc2hhaEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IlZnS0lSemJycHRON2ZSM19FMk1PY2ciLCJleHAiOjE1MjQ2NTA2ODgsImlzcyI6Imh0dHBzOi8vYWNjb3VudHMuZ29vZ2xlLmNvbSIsImlhdCI6MTUyNDY0NzA4OH0.JvCAvls3MDhOuo6KhZFdMkKrbEsh0QekB9lapcBShFhudnTEDEl0f_a0N7MZ_B1XQnx68DMb79HfQp56e-QJ9WJgIV1lsEJaPl6OCjHJphq7YWGk_r6Cic7sgiJtcxME6arUyPOAEbuEBPTwlW_rCBjcv3MBGyaJ4FGDfpEl6rmTPkMp3hT6_Y7r9eu5m7n6GDEHJ2st5SQU0eRQdrKNSyYK_mpm87HdX7emFMdFbEJh2FZxs1Miruy-vra0cq2TPU-U3pg5vuD6dA6Ee3Cgvtvq4cZbmO_Y7i7Gcb2o0PDJ4NuYfU7i_TUYh4b67-ZNmKYlDD-s37BdGteeWHRt1w\",\"created\":1524647088}', '\"1\\/2sdiDNA_TooBJDLicY8j_nybExA0BHQ-SYTR2gYIgso\"', '', '1', null, null, 'user', null, '2018-04-25 02:04:49', '2018-04-25 02:04:49');
