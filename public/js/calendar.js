
$(function() {

$('#calendar').fullCalendar({

  eventSources: [

    // your event source
    {
      url: '/api/getEvents',
      type: 'POST',
      data: {
        id: 13,
        month: 4,
        calendar:true
      },
      error: function(data) {
          console.log(data);
        alert('there was an error while fetching events!');
      },
      color: 'yellow',   // a non-ajax option
      textColor: 'black' // a non-ajax option
    }

    // any other sources...

  ]

});

});