<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Appointment checker') }}</title>

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/admin.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/admin/styles.css') }}">
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans" >
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" >
    <style>
        .total {
            position: absolute;
            border-radius: 100%;
            background-color: #D8455F;
            color: #FFFFFF;
            width: 27px;
            height: 27px;
            display: inline-table;
            right: 6px;
            top: 50%;
            -webkit-transform: translate(0, -50%);
            -ms-transform: translate(0, -50%);
            transform: translate(0, -50%);
        }
        .total span {
            display: table-cell;
            vertical-align: middle;
            font-size: 12px;
            position: relative;
            text-align: center;
        }
    </style>
    
</head>
<body>
    <div id="app" v-cloak="">
        <header class="MAIN-FIX-TOP">
            <div class="MAIN-FIX-TOP-FIX">
                <div class="main-fix-top-fix-pad">
                    <div class="top-menu-place">
                        <!-- Collapsed Hamburger -->
                        <div type="button" class="sidebar-toggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                                        <!-- Authentication Links -->
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->first_name . " " . Auth::user()->last_name }}
                                    <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
        <div class="MAIN-CARCASS">
            <div class="MAIN-FIX-CARCASS">
                <div class="MAIN-FIX-LEFT">
                    <div class="LEFT-BAR-SCROLL-PLACE">
                        <div class="LEFT-BAR-pad">
                            <div class="logo-place">
                                <a href="{{ env('SITE_URL') }}" target="_blank"><img src="{{ url('img/logo.svg') }}" alt="logo"> </a>
                            </div>
                            <div class="catergory-place">
                            
                                @if(Auth::user())
                                    <ul class="sidebar-nav">
                                        @if(Auth::user()->role == 'admin')                                       
                                            
                                        @elseif (Auth::user()->role == 'employee')
                                            <li class="{{ Request::is('dashboard') || Request::is('dashboard/*') ? 'active-menu-sidebar' : '' }}">
                                                <a href="/dashboard">
                                                    Dashboard
                                                </a>
                                            </li>
                                        @endif
                                    </ul>
                                @endif

                            </div>
                        </div>
                        <p class="copyright">
                            Copyright 2017 © Appointment Checker All Rights Reserved
                        </p>
                    </div>
                </div>
                <div class="MAIN-FIX-CARCASS-PAD">
                    <div class="MAIN-FIX-CENTER">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/scripts.js') }}"></script>
    <script src="{{ asset('js/bootstrap.js') }}"></script>

    
    
    @if(explode('/', Request::path())[0] && file_exists("js/" . explode('/', Request::path())[0] . ".js"))
        <script src="{{ asset('js/' . (explode('/', Request::path())[0]) . '.js?v=' . date('YmdH')) }}"></script>
	@endif
</body>
</html>
