<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link type="text/css" rel="stylesheet" href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="{{ asset('css/admin.css') }}" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="/css/admin/styles.css" rel="stylesheet">
</head>
<body>
   <div id="app" v-cloak="">
        @yield('content')
    </div>
      

    

    <!-- Scripts -->
    <script src="/js/moment.min.js"></script>
    <script src="{{ asset('js/app.js') }}"></script>
    @if(explode('/', Request::path())[0] && file_exists("js/" . explode('/', Request::path())[0] . ".js"))
        <script src="{{ asset('js/' . (explode('/', Request::path())[0]) . '.js?v=' . date('YmdH')) }}"></script>
	@endif
</body>
</html>
