
<!------ Include the above in your HEAD tag ---------->
@extends('layouts.app')

@section('content')
<div class="col-md-6">
    <form class="form-horizontal" action='{{ route('register') }}' method="POST">
         {{ csrf_field() }}
      <fieldset>
        <div id="legend">
          <legend class="">Register</legend>
        </div>

        <div class="control-group {{ $errors->has('first_name') ? ' has-error' : '' }}">
          <!-- first_name -->
          <label class="control-label"  for="first_name">First name</label>
          <div class="controls">
            <input type="text" id="first_name" name="first_name" placeholder=""  value="{{ old('first_name') ? old('first_name') : '' }}" class="input-xlarge" required>
            <p class="help-block">First name can contain any letters or numbers, without spaces</p>
            @if ($errors->has('first_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('first_name') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="control-group {{ $errors->has('last_name') ? ' has-error' : '' }}">
          <!-- last_name -->
          <label class="control-label"  for="last_name">Last name</label>
          <div class="controls">
            <input type="text" id="last_name" name="last_name" placeholder=""  value="{{ old('last_name') ? old('last_name') : '' }}" class="input-xlarge" required>
            <p class="help-block">Last name can contain any letters or numbers, without spaces</p>
            @if ($errors->has('last_name'))
                <span class="help-block">
                    <strong>{{ $errors->first('last_name') }}</strong>
                </span>
            @endif
          </div>
        </div>

        <div class="control-group {{ $errors->has('company') ? ' has-error' : '' }}">
          <!-- company -->
          <label class="control-label"  for="company">Company</label>
          <div class="controls">
            <input type="text" id="company" name="company" placeholder="" value="{{ old('company') ? old('company') : '' }}" class="input-xlarge" required>
            <p class="help-block">Company name can contain any letters or numbers, without spaces</p>
            @if ($errors->has('company'))
                <span class="help-block">
                    <strong>{{ $errors->first('company') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="control-group">
          <!-- Button -->
          <div class="controls">
            <button class="btn btn-success">Register</button>
          </div>
        </div>
      </fieldset>
    </form>
</div>
<div class="col-md-6">
    <form class="form-horizontal" action='{{ route('register') }}' method="POST">
         {{ csrf_field() }}
      <fieldset>
        <div id="legend">
          <legend class="">Login</legend>
        </div>

        <div class="control-group">
          <!-- Button -->
          <div class="controls">
            <!--<button class="btn btn-success">Login</button>-->
            <a href=" {{ $auth_url }}" class="btn btn-success">Login</a>
          </div>
        </div>
      </fieldset>
    </form>
</div>
@endsection